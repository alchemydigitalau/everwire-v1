<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::model('article', 'Article');
Route::model('channel', 'Channel');

Route::get('/', 'HomeController@index');

//Route::any('/', 'ZencoderController@notify');

// Auth
Route::group(array('prefix' => 'auth'), function() {
	Route::get('/login', 'AuthController@login');
	Route::post('/login', 'AuthController@handleLogin');
	Route::get('/logout', 'AuthController@logout');

	Route::get('/signup', 'AuthController@signup');
	Route::post('/signup', 'AuthController@handleSignup');
	/*Route::get('/signin', 'AuthController@signin');
	Route::post('/signin', 'AuthController@handleSignin');
	Route::get('/signout', 'AuthController@signout');
	Route::get('/signup', 'AuthController@signup');
	Route::post('/signup', 'AuthController@handleSignup');

	Route::get('/request', 'AuthController@request');
	Route::post('/request', 'AuthController@handleRequest');

	Route::get('/reset/{token}', 'AuthController@reset');
	Route::post('/reset/{token}', 'AuthController@handleReset');*/

	Route::get('/facebook', 'AuthController@facebook');
	Route::any('/facebook/callback', 'AuthController@facebookCallback');	
});

Route::get('channels', 'ChannelController@index');

Route::get('channels/yours', 'ChannelController@yourChannels');

Route::group(array('prefix' => 'channel'), function() {
	Route::get('/create', 'ChannelController@create');
	Route::post('/create', 'ChannelController@handleCreate');

	Route::get('/{channel}/edit', 'ChannelController@edit');
	Route::post('/{channel}/edit', 'ChannelController@handleEdit');

	Route::get('/{channel}', 'ChannelController@show');

});

Route::get('articles/yours', 'ArticleController@yourArticles');

Route::group(array('prefix' => 'article'), function() {
	
	Route::get('/create', array('as' => 'create_article', 'uses' => 'ArticleController@create'));
	Route::post('/create', 'ArticleController@handleCreate');

	Route::get('/create/slideshow', array('as' => 'create_slideshow', 'uses' => 'GalleryController@create'));
	Route::post('/create/slideshow', 'GalleryController@handleCreate');

	Route::get('/create/video', array('as' => 'create_video', 'uses' => 'VideoController@create'));
	Route::post('/create/video', 'VideoController@handleCreate');

	Route::get('/{article}/edit', 'ArticleController@edit');
	Route::post('/{article}/edit', 'ArticleController@handleEdit');

	Route::get('/{article}/edit', 'ArticleController@edit');

	Route::post('/{article}/comment', 'ArticleController@handleComment');

	Route::get('/{article}', 'ArticleController@show');
});



Route::group(array('prefix'	=> 'flag'), function() {

	Route::post('/create', 'FlagController@handleCreate');


});



Route::group(array('prefix'	=> 'zencoder'), function() {

	Route::any('/notify', 'ZencoderController@notify');


});

Route::group(array('prefix'	=> 'upload'), function() {

	Route::post('/image', 'UploadController@handleImage');
	Route::post('/video', 'UploadController@handleVideo');

});

Route::group(array('prefix'	=> 'account'), function() {

	Route::get('/profile', 'AccountController@profile');
	Route::post('/profile', 'AccountController@handleProfile');

	Route::get('/password', 'AccountController@password');
	Route::post('/password', 'AccountController@handlePassword');

});


Route::group(array('prefix'	=> 'admin'), function() {

	Route::get('/flags', 'FlagController@index');


});

Route::group(array('prefix'	=> 'page'), function() {

	Route::any('/about', 'PageController@about');
	Route::any('/terms', 'PageController@terms');


});