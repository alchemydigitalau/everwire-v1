<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('places', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->string('full_name')->unique();

            $table->decimal('latitude', 14, 10);
            $table->decimal('longitude', 14, 10);

            $table->decimal('bounds_north', 14, 10)->nullable()->default(null);
            $table->decimal('bounds_south', 14, 10)->nullable()->default(null);
            $table->decimal('bounds_east', 14, 10)->nullable()->default(null);
            $table->decimal('bounds_west', 14, 10)->nullable()->default(null);

            $table->index('full_name');
		});

		Schema::create('place_user', function(Blueprint $table)
		{
			$table->integer('place_id')->unsigned();
			$table->integer('user_id')->unsigned();
		});

		Schema::table('articles', function(Blueprint $table)
		{
			$table->foreign('place_id')->references('id')->on('places');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('places', function(Blueprint $table)
		{
			//
		});

		Schema::table('articles', function(Blueprint $table)
		{
			$table->dropForeign('articles_place_id_foreign');
		});

		Schema::drop('places');
		Schema::drop('place_user');
	}

}