<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('articles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('channel_id')->unsigned();
			$table->integer('place_id')->unsigned();
			$table->boolean('published')->default(FALSE);
			$table->string('title');
			$table->string('subtitle')->nullable()->default(null);

			$table->integer('payload_id')->unsigned()->nullable()->default(null);
			$table->string('payload_type')->nullable()->default(null);

			$table->text('body_md')->nullable()->default(null);
			$table->text('body_html')->nullable()->default(null);
			
			$table->timestamp('published_at')->nullable()->default(null);
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('user_id')->references('id')->on('users');
			
			$table->foreign('channel_id')->references('id')->on('channels');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('articles', function(Blueprint $table)
		{
			$table->dropForeign('articles_user_id_foreign');
			$table->dropForeign('articles_channel_id_foreign');
		});

		Schema::drop('articles');
	}

}
