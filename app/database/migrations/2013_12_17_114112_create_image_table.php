<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('images', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('gallery_id')->unsigned()->nullable()->default(null);
            $table->string('upload_hash')->nullable()->default(null);
            $table->string('file_name')->nullable()->default(null);
			$table->string('original_name')->nullable()->default(null);
			$table->string('file_type')->nullable()->default(null);
            $table->string('caption')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('gallery_id')->references('id')->on('galleries');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('images', function(Blueprint $table)
		{
			$table->dropForeign('images_user_id_foreign');
			$table->dropForeign('images_gallery_id_foreign');
		});

		Schema::drop('images');
	}

}