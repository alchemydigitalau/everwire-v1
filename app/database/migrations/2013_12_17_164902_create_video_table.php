<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('videos', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id')->unsigned();
            //$table->integer('article_id')->unsigned()->nullable()->default(null);
            $table->string('upload_hash')->nullable()->default(null);
            
            $table->string('file_name')->nullable()->default(null);

            $table->string('url_preview_thumb')->nullable()->default(null);

            $table->string('url_mp4_high')->nullable()->default(null);
            $table->string('url_mp4_low')->nullable()->default(null);
            $table->string('url_webm')->nullable()->default(null);
            $table->string('url_ogg')->nullable()->default(null);

            $table->integer('job_id')->nullable()->default(null);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
            //$table->foreign('article_id')->references('id')->on('articles');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('videos', function(Blueprint $table)
		{
			$table->dropForeign('videos_user_id_foreign');
			//$table->dropForeign('videos_article_id_foreign');
		});

		Schema::drop('videos');
	}

}