<?php


class UserTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        $faker = Faker\Factory::create('en_GB');

        $faker->addProvider(new Faker\Provider\Internet($faker));
        $faker->addProvider(new Faker\Provider\Company($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));

        $total_articles = 0;

        

        $places = array(
                        'Brixton, United Kingdom',
                        'Clapham Junction, London Borough of Wandsworth, United Kingdom',
                        'Clapham Common, London Borough of Lambeth, United Kingdom',
                        'Griffin, Queensland, Australia'
                        );

        $adapter = new \Geocoder\HttpAdapter\CurlHttpAdapter();
        $chain = new \Geocoder\Provider\ChainProvider(
                                                        array(
                                                            new \Geocoder\Provider\GoogleMapsProvider($adapter),
                                                        )
                                                    );

        $geocoder = new \Geocoder\Geocoder();

        $geocoder->registerProvider($chain);

        foreach ($places as $place) {
            // Try to geocode.
            try {
                $geocode = $geocoder->geocode($place);
                
                $p = new Place;
                $p->full_name = $place;
                $p->latitude = $geocode->getLatitude();
                $p->longitude = $geocode->getLongitude();

                $p->save();

            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

        $u = User::create(array(
                                'name'  => 'Phil Stephens',
                                'email' => 'phil@othertribe.com',
                                'password'  => 'pcr34t366',
                                'admin'     => true
                                ));

        $c = Channel::create(array(
                                    'user_id'   => $u->id,
                                    'name'      => 'Phil\'s Channel'
                                    ));

        $u->places()->attach(4);

        $u = User::create(array(
                                'name'  => 'Colin Livingstone',
                                'email' => 'colinlivingstone@live.co.uk',
                                'password'  => 'paris',
                                'admin'     => true
                                ));

        $c = Channel::create(array(
                                    'user_id'   => $u->id,
                                    'name'      => 'Colin\'s Channel'
                                    ));


        for ($n = 0; $n < 10; $n++)
        {
            

            $u = User::create(array(
                                    'first_name'  => $faker->firstName,
                                    'last_name'  => $faker->lastName,
                                    'email' => $faker->email,
                                    'password'  => str_random(8)
                                    ));

            $name = md5(uniqid()) .'.jpg';
            $destination_path = 'media_server/img/uploads/u/' . $u->id;

            if (!file_exists($destination_path)) {
                mkdir($destination_path, 0777, true);
            }

            file_put_contents($destination_path  . '/' . $name, file_get_contents("http://lorempixel.com/200/200"));

            $u->avatar = Config::get('media.media_server') . '/img/50x50/uploads/u/' . $u->id . '/' . $name;
            $u->save();

            $u->places()->attach(rand(1,3));

            $c = Channel::create(array(
                                    'user_id'   => $u->id,
                                    'name'      => $faker->catchPhrase()
                                    ));

            $articles = rand(2, 20);

            $total_articles += $articles;
            
            for($i = 0; $i <= $articles; $i++)
            {
                $article = new Article;

                $article->channel_id = $c->id;

                $article->place_id = rand(1,3);

                $article->title = $faker->sentence(rand(3, 10));

                $article->subtitle = $faker->sentence(rand(10, 20));

                $article->published = TRUE;

                $article->published_at = $faker->dateTimeBetween('-4 months');

                if(rand(0,6) > 0)
                {
                    $article->body_md = implode("\n\n", $faker->paragraphs(rand(4, 12)));

                    $u->articles()->save($article);
                } else
                {
                    $article->body_md = implode("\n\n", $faker->paragraphs(rand(1, 4)));

                    $article = $u->articles()->save($article);

                    $gallery = new Gallery;

                    $gallery->user_id = $u->id;

                    $gallery->save();

                    $gallery->articles()->save($article);
                    //$gallery->title = $article->title;

                  
                    //$gallery = $article->galleries()->save($gallery);

                    $total = rand(3,6);

                    $uid = md5(uniqid() . $u->id);

                    for($g = 0; $g < $total; $g++)
                    {
                        $name = md5(uniqid()) .'.jpg';
                        $destination_path = 'media_server/img/uploads/a/' . $uid;

                        if (!file_exists($destination_path)) {
                            mkdir($destination_path, 0777, true);
                        }

                        file_put_contents($destination_path  . '/' . $name, file_get_contents("http://lorempixel.com/1024/768"));

                        $image = new Image;

                        $image->user_id = $u->id;
                        $image->upload_hash = $uid;
                        $image->file_name = $image->original_name = $name;
                        $image->file_type = 'image/jpeg';
                        $image->caption = $faker->sentence(rand(10, 20));

                        $gallery->images()->save($image);
                    }
                }




            }
        }

        $total_comments = 10 * $total_articles;
        
        for ($n = 0; $n < $total_comments; $n++)
        {
            $comment = new Comment;

            $comment->article_id = rand(1, $total_articles);
            $comment->user_id = rand(1, 10);
            $comment->body = $faker->paragraph(rand(1, 4));

            $comment->save();
        }
    }
}