<?php

class FlagController extends BaseController {

	public function index()
	{
		$flags = Flag::with('article', 'user')->get();

		return View::make('flag/index', compact('flags'));
	}

	public function handleCreate()
	{
		$validator = Validator::make(Input::all(), array(
														'reason'		=> 'required'
										            	));

		if($validator->passes())
		{
			$flag = new Flag;

			$flag->article_id = Input::get('article_id');
			$flag->user_id = Input::get('user_id');
			$flag->reason = Input::get('reason');
			$flag->inappropriate = Input::has('inappropriate') && Input::get('inappropriate');
			$flag->inaccurate = ! $flag->inappropriate;

			$flag->save();


			return Redirect::action('ArticleController@show', Input::get('article_id'));
		} 

		return Redirect::action('ArticleController@show', Input::get('article_id'));
	}

}