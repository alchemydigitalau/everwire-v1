<?php

class VideoController extends BaseController {

	public function create()
	{
		$channels = array();

		foreach(Auth::user()->channels as $c)
		{
			$channels[$c->id] = $c->name;
		}

		$upload_hash = md5(uniqid() . Auth::user()->id);

		return View::make('video/create', compact('channels', 'upload_hash'));
	}

	public function handleCreate()
	{
		$validator = Validator::make(Input::all(), array(
														'title'		=> 'required',
														'place_id'	=> 'required'
										            	));

		if($validator->passes())
		{
			$article = new Article;

			$article->channel_id = Input::get('channel_id');
			$article->place_id = Input::get('place_id');
			$article->title = Input::get('title');
			$article->subtitle = Input::get('subtitle');
			$article->body_md = Input::get('body_md');
			
			$article->published = Input::has('published');

			if($article->published)
			{
				$article->published_at = Carbon\Carbon::now()->toDateTimeString();
			}

			$article = Auth::user()->articles()->save($article);

			// Now for the video
			$videos = Video::whereUploadHash(Input::get('upload_hash'))->get();

	       	foreach($videos as $video)
	       	{
	       		$article->videos()->save($video);
	       	}


			return Redirect::action('ArticleController@show', $article->id);
		} else
		{
			$messages = $validator->messages();
		}

		$channels = array();

		foreach(Auth::user()->channels as $c)
		{
			$channels[$c->id] = $c->name;
		}

		$upload_hash = Input::get('upload_hash');

		return View::make('video/create', compact('channels', 'upload_hash', 'messages'));
	}

	public function notify()
	{
		$vars = Input::all();

		$job = $vars['job'];

		$video = Video::whereJobId($vars['job']['id'])->first();

		if( ! empty($video))
		{
			foreach($vars['outputs'] as $output)
			{
				$label = 'url_' . $output['label'];
				$video->$label = $output['url'];
			}

			$video->save();

			Log::info($vars);
		}
	}

	
}