<?php

class ZencoderController extends BaseController {

	public function notify()
	{
		$vars = Input::all();

		

		$job = $vars['job'];

		$video = Video::whereJobId($vars['job']['id'])->first();

		if( ! empty($video))
		{
			foreach($vars['outputs'] as $output)
			{
				$label = 'url_' . $output['label'];
				$video->$label = $output['url'];

				if(isset($output['thumbnails']))
				{
					$video->url_preview_thumb = $output['thumbnails'][0]['images'][0]['url'];
				}
			}

			$video->save();

			Log::info($vars);
		}
	}

	
}