<?php

class AccountController extends BaseController {

	public function profile()
	{
		return View::make('account/profile');
	}

	public function handleProfile()
	{
		$validator = Validator::make(Input::all(), array(
														'first_name'	=> 'required',
										                'email'			=> 'required|email'
									            		));
	    if ($validator->passes())
	    {
	       	Auth::user()->update(Input::all());

			return Redirect::action('AccountController@profile');
	    }
	    else
	    {
	        $messages = $validator->messages();
	    }

	    return View::make('account/profile', compact('messages'));
	}

	public function password()
	{
		return View::make('account/password');
	}

	public function handlePassword()
	{
		$validator = Validator::make(Input::all(), array(
														'password'	=> 'required|same:password_confirmation|min:6'
										            	));
			
	    if ($validator->passes())
	    {
            Auth::user()->password = Input::get('password');

            Auth::user()->save();
            return Redirect::action('AccountController@password');
	    }
	    else
	    {
	        $messages = $validator->messages();
	    }

	    return View::make('account/password', compact('messages'));
	}
}