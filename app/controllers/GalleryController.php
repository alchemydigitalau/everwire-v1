<?php

class GalleryController extends BaseController {

	public function create()
	{
		$channels = array();

		foreach(Auth::user()->channels as $c)
		{
			$channels[$c->id] = $c->name;
		}

		$upload_hash = md5(uniqid() . Auth::user()->id);

		return View::make('gallery/create', compact('channels', 'upload_hash'));
	}

	public function handleCreate()
	{
		$validator = Validator::make(Input::all(), array(
														'title'		=> 'required',
														'place_id'	=> 'required'
										            	));

		if($validator->passes())
		{
			$article = new Article;

			$article->channel_id = Input::get('channel_id');
			$article->place_id = Input::get('place_id');
			$article->title = Input::get('title');
			$article->subtitle = Input::get('subtitle');
			$article->body_md = Input::get('body_md');
			
			$article->published = Input::has('published');

			if($article->published)
			{
				$article->published_at = Carbon\Carbon::now()->toDateTimeString();
			}

			$article = Auth::user()->articles()->save($article);

			// Now for the images
			$gallery = new Gallery;

			$gallery->user_id = Auth::user()->id;
			$gallery->title = $article->title;

			$gallery = $article->galleries()->save($gallery);


			$images = Image::whereUploadHash(Input::get('upload_hash'))->get();

	       	foreach($images as $image)
	       	{
	       		$gallery->images()->save($image);
	       	}

			return Redirect::action('ArticleController@show', $article->id);
		} else
		{
			$messages = $validator->messages();
		}

		$channels = array();

		foreach(Auth::user()->channels as $c)
		{
			$channels[$c->id] = $c->name;
		}

		$upload_hash = Input::get('upload_hash');

		return View::make('gallery/create', compact('channels', 'upload_hash', 'messages'));
	}

	
}