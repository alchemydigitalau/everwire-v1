<?php

class ArticleController extends BaseController {

	public function yourArticles()
	{
		$articles = Auth::user()->articles()->orderBy('created_at', 'desc')->get();

		return View::make('article/yourArticles', compact('articles'));
	}

	public function show(Article $article)
	{
		if( ! $article->published)
		{
			return Redirect::action('ChannelController@show', $article->channel_id);
		}

		$comments = $article->load('comments');

		switch($article->payload_type)
		{
			case 'Video':
				return View::make('video/show', compact('article', 'comments'));
				break;

			case 'Gallery':
				return View::make('gallery/show', compact('article', 'comments'));
				break;

			default:
				return View::make('article/show', compact('article', 'comments'));
				break;
		}
	}

	public function create()
	{
		$channels = array();

		foreach(Auth::user()->channels as $c)
		{
			$channels[$c->id] = $c->name;
		}

		$uid = md5(uniqid() . Auth::user()->id);

		return View::make('article/create', compact('channels', 'uid'));
	}

	public function handleCreate()
	{
		$validator = Validator::make(Input::all(), array(
														'title'		=> 'required',
														'body_md'	=> 'required',
														'place_id'	=> 'required'
										            	));

		if($validator->passes())
		{
			$article = new Article;

			$article->channel_id = Input::get('channel_id');
			$article->place_id = Input::get('place_id');
			$article->title = Input::get('title');
			$article->subtitle = Input::get('subtitle');
			$article->body_md = Input::get('body_md');
			$article->published = Input::has('published');

			if($article->published)
			{
				$article->published_at = Carbon\Carbon::now()->toDateTimeString();
			}

			$article = Auth::user()->articles()->save($article);

			return Redirect::action('ArticleController@show', $article->id);
		} else
		{
			$messages = $validator->messages();
		}

		$channels = array();

		foreach(Auth::user()->channels as $c)
		{
			$channels[$c->id] = $c->name;
		}

		return View::make('article/create', compact('channels', 'messages'));
	}

	public function edit(Article $article)
	{
		$channels = array();

		foreach(Auth::user()->channels as $c)
		{
			$channels[$c->id] = $c->name;
		}

		return View::make('article/edit', compact('article', 'channels'));
	}

	public function handleEdit(Article $article)
	{
		$validator = Validator::make(Input::all(), array(
														'title'		=> 'required',
														'body_md'	=> 'required'
										            	));

		if($validator->passes())
		{
			$article->channel_id = Input::get('channel_id');
			$article->title = Input::get('title');
			$article->subtitle = Input::get('subtitle');
			$article->body_md = Input::get('body_md');
			$article->published = Input::has('published');

			if($article->published)
			{
				$article->published_at = Carbon\Carbon::now()->toDateTimeString();
			} else
			{
				$article->published_at = null;
			}

			$article->save();

			return Redirect::action('ArticleController@yourArticles');
		} else
		{
			$messages = $validator->messages();
		}

		$channels = array();

		foreach(Auth::user()->channels as $c)
		{
			$channels[$c->id] = $c->name;
		}

		return View::make('article/edit', compact('article', 'channels', 'messages'));
	}

	public function handleComment(Article $article)
	{
		$comment = new Comment;
		$comment->user_id = Input::get('user_id');
		$comment->body = Input::get('body');

		$comment = $article->comments()->save($comment);

		return View::make('partials/comment', compact('comment'));
	}
}