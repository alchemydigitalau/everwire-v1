<?php

class UploadController extends BaseController {



	public function handleImage()
	{

		$user = User::findOrFail(Input::get('user'));

		$image = new Image;

		$image->user_id = $user->id;

		$image->upload_hash = Input::get('upload_hash');

		if($file = Input::file('file'))
		{
			// It's an image upload

			$destinationPath = '../media_server/img/uploads/a/' . Input::get('upload_hash');
		
			$filename = md5(uniqid()) . '.' . $file->getClientOriginalExtension();

			$image->file_name = $filename;
			$image->original_name = $file->getClientOriginalName();
			$image->file_type = $file->getMimeType();

			if( Input::file('file')->move($destinationPath, $filename) ) 
			{
				if(Input::has('gallery'))
				{
					$gallery = Gallery::find(Input::get('gallery'));

					$gallery->images()->save($image);
				} else
				{
					$image->save();
				}

			   	return Response::json('success', 200);
			} else 
			{
			   	return Response::json('error', 400);
			}
		}
	}

	public function handleVideo()
	{
		$user = User::findOrFail(Input::get('user'));

		$video = new Video;

		$video->user_id = $user->id;

		$video->upload_hash = Input::get('upload_hash');

		if($file = Input::file('file'))
		{
			// It's an image upload

			$destinationPath = '../media_server/img/uploads/a/' . $video->upload_hash;
		
			$file_raw_name = md5(uniqid());
			$filename = $file_raw_name . '.' . $file->getClientOriginalExtension();

			$video->file_name = $filename;

			if( Input::file('file')->move($destinationPath, $filename) ) 
			{
				$video->save();

				$prefix = 'http://media.everwire.net/img/uploads/a/';

				$notify = action('ZencoderController@notify');

				if(App::environment() != 'production')
				{
					$s3 = AWS::get('s3');
					
					$s3->putObject(array(
					    'Bucket'     	=> 'everwire',
					    'Key'        	=> $video->upload_hash . '/' . $filename,
					    'SourceFile' 	=> $destinationPath . '/' . $filename,
					    'ACL'			=> 'public-read',
					));

					$prefix = 's3://everwire/';
					$notify = 'http://zencoderfetcher/';
				}

				$zencoder = new Services_Zencoder(Config::get('zencoder.api_key'));

				$job = array(
						'input'	=> $prefix . $video->upload_hash . '/' . $filename,
						'notifications'	=> array($notify),
						'outputs'	=> array(
							array(
								'label'			=> 'mp4_high',
								'url'			=> 's3://everwire/e/' . $video->upload_hash . '/' . $file_raw_name . '.mp4',
								'h264_profile'	=> 'high',
								'public'		=>  true,
								'thumbnails'	=> array(
														'number'	=> 1,
														'base_url'	=> 's3://everwire/e/' . $video->upload_hash . '/',
														'public'	=> true
														)
								),
							array(
								'label'		=> 'webm',
								'url'		=> 's3://everwire/e/' . $video->upload_hash . '/' . $file_raw_name . '.webm',
								'public'	=>  true
								),
							array(
								'label'		=> 'ogg',
								'url'		=> 's3://everwire/e/' . $video->upload_hash . '/' . $file_raw_name . '.ogg',
								'public'	=>  true,
								),
							array(
								'label'		=> 'mp4_low',
								'url'		=> 's3://everwire/e/' . $video->upload_hash . '/' . $file_raw_name . '-mobile.mp4',
								'size'		=> '640x480',
								'public'	=>  true,
								)

							)
						);

				$encoding_job = $zencoder->jobs->create($job);


				$video->job_id = $encoding_job->id;

				$video->save();

			   	return Response::json('success', 200);
			} else 
			{
			   	return Response::json('error', 400);
			}
		} else
		{
			return Response::json(ini_get('post_max_size'), 400);
		}
	}

}