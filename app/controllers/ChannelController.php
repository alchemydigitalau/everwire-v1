<?php

class ChannelController extends BaseController {

	public function index()
	{
		$channels = Channel::all();

		return View::make('channel/index', compact('channels'));
	}

	public function show(Channel $channel)
	{
		$articles = $channel->articles()->wherePublished(true)->orderBy('published_at', 'desc')->get();

		return View::make('channel/show', compact('channel', 'articles'));
	}

	public function yourChannels()
	{
		$channels = Auth::user()->channels;

		return View::make('channel/yourChannels', compact('channels'));
	}

	public function create()
	{
		return View::make('channel/create');
	}

	public function handleCreate()
	{
		$validator = Validator::make(Input::all(), array(
														'name'		=> 'required'
										            	));

		if($validator->passes())
		{
			$channel = new Channel;

			$channel->name = Input::get('name');
			

			$channel = Auth::user()->channels()->save($channel);

			return Redirect::action('ChannelController@show', $channel->id);
		} else
		{
			$messages = $validator->messages();
		}

		return View::make('channel/create', compact('messages'));
	}

	public function edit(Channel $channel)
	{
		return View::make('channel/edit', compact('channel'));
	}

	public function handleEdit(Channel $channel)
	{
		$validator = Validator::make(Input::all(), array(
														'name'		=> 'required'
										            	));

		if($validator->passes())
		{

			$channel->name = Input::get('name');
			

			$channel->save();

			return Redirect::action('ChannelController@show', $channel->id);
		} else
		{
			$messages = $validator->messages();
		}

		return View::make('channel/edit', compact('channel', 'messages'));
	}
}