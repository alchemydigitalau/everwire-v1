<?php

use Illuminate\Support\MessageBag;

class AuthController extends BaseController {

	public function login()
	{
		return View::make('auth/login');
	}

	public function handleLogin()
	{
		$validator = Validator::make(Input::all(), array(
										                'email'		=> 'required|email',
										                'password' 	=> 'required'
										            	));
	    if ($validator->passes())
	    {
	        $credentials = array(
			                    'email' 	=> Input::get('email'),
			                    'password' 	=> Input::get('password')
	                			);
            
            if (Auth::attempt($credentials))
            {
                return Redirect::intended('/');
            } else
            {
            	$messages = new MessageBag(array(
	                							'password' => array(
		                    										'That email/password combination is invalid.'
		                											)
	            								));
            }
	    }
	    else
	    {
	        $messages = $validator->messages();
	    }

	    return View::make('auth/login', compact('messages'));
	}

	public function logout()
	{
		Auth::logout();

		return Redirect::action('HomeController@index');
	}

	public function signup()
	{
		return View::make('auth/signup');
	}

	public function handleSignup()
	{
		$validator = Validator::make(Input::all(), array(
										                'name'		=> 'required',
										                'email'		=> 'required|email|unique:users',
										                'password' 	=> 'required|min:6',
										                'accept_terms'	=> 'accepted'
										            	));
	    if ($validator->passes())
	    {
	    	
	    	$user = User::create(Input::all());

	    	$this->postSignup($user);

	    	Auth::login($user);

	        return Redirect::action('AccountController@profile');
	    }
	    else
	    {
	        $messages = $validator->messages();
	    }

	    return View::make('auth/signup', compact('messages'));
	}

	private function postSignup(User $user)
	{
		$channel = new Channel;

        $channel->name = $user->first_name . ' ' . $user->last_name .'\'s Channel';

        $user->channels()->save($channel);
	}

	/**
	* Facebook Signup/Login methods
	*/

	public function facebook()
	{
		$facebook = new Facebook(Config::get('facebook'));
		$params = array(
						'redirect_uri'	=> action('AuthController@facebookCallback'),
						'scope'			=> 'email'
						);

		return Redirect::to($facebook->getLoginUrl($params));
	}

	public function facebookCallback()
	{
		$code = Input::get('code');
    	
    	if (strlen($code) == 0) return Redirect::action('HomeController@index')->with('message', 'There was an error communicating with Facebook');

    	$facebook = new Facebook(Config::get('facebook'));
    	$uid = $facebook->getUser();
 
 		if ($uid == 0) return Redirect::action('HomeController@index')->with('message', 'There was an error');
 
	    $me = $facebook->api('/me');

	    $profile = Profile::whereUid($uid)->first();
	    
	    if (empty($profile)) {
	 		$user = User::whereEmail($me['email'])->first();

	 		if(empty($user))
	 		{
	 			$user = new User;
	 			
	 			$user->first_name = $me['first_name'];
		        $user->last_name = $me['last_name'];
		        $user->email = $me['email'];
		        $user->avatar = 'https://graph.facebook.com/' . $me['username'] . '/picture?type=large';

		        $user->save();

		        // Get the FB profile pic
		        /*$name = md5(uniqid()) .'.jpg';
	            $destination_path = '../media_server/img/uploads/u/' . $user->id;

	            if ( ! file_exists($destination_path)) {
	                mkdir($destination_path, 0777, true);
	            }

	            $url = 'https://graph.facebook.com/' . $me['username'] . '/picture?type=large';

	           	if (function_exists('curl_exec')) 
	           	{
		            // use cURL
		            $fp = fopen($destination_path  . '/' . $name, 'w');
		            $ch = curl_init($url);
		            curl_setopt($ch, CURLOPT_FILE, $fp);
		            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE); // Needed because Facebook redirects to the actual file
		            $success = curl_exec($ch);
		            curl_close($ch);
		            fclose($fp);
		        } elseif (ini_get('allow_url_fopen')) 
		        {
		            // use remote fopen() via copy()
		            $success = copy($url, $destination_path  . '/' . $name);
		        } else
		        {
		        	$success = file_put_contents($destination_path  . '/' . $name, file_get_contents($url));
		        }

		        if($success)
		        {
		        	$user->avatar = $name;
		            $user->save();
		        }*/

		        $this->postSignup($user);
	 		}
	 
	        $profile = new Profile();
	        $profile->uid = $uid;
	        $profile->username = $me['username'];
	        $profile = $user->profiles()->save($profile);
	    }
	 
	    $profile->access_token = $facebook->getAccessToken();
	    $profile->save();
	 
	    $user = $profile->user;
	 
	    Auth::login($user);
	 
	    return Redirect::action('HomeController@index')->with('message', 'Logged in with Facebook');
	}
}