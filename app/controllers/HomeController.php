<?php

class HomeController extends BaseController {

	public function index()
	{
		if($place = Input::get('p'))
		{
			// Does the place exist in the database?


			// If not, retrieve it from Geo provider and insert into database

			// Now do the search based on that

			$articles = array();

		} else if(Auth::check())
		{
			// Search based on user's preferences
			$articles = array();
		} else
		{
			// Get the latest articles based on Geo IP (broad)
			
			$articles = Article::wherePublished(true)->orderBy('published_at', 'desc')->paginate(10);
		}

		return View::make('home/index', compact('articles'));
	}
}