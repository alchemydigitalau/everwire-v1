<?php

class Video extends Eloquent 
{
	protected $softDelete = true;
    protected $guarded = array('id');

    /**
	* Relationships
	*/
    public function user()
    {
        return $this->belongsTo('User');
    }

    public function articles()
    {
        return $this->morphMany('Article', 'payload');
    }


}