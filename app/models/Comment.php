<?php

class Comment extends Eloquent 
{
	protected $softDelete = true;
    protected $guarded = array('id');

    /**
	* Relationships
	*/
	public function user()
    {
        return $this->belongsTo('User');
    }

    public function article()
    {
        return $this->belongsTo('Article');
    }


}