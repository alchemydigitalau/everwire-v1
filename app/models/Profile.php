<?php

class Profile extends Eloquent 
{
	protected $softDelete = true;
    protected $guarded = array('id');

    /**
	* Relationships
	*/
	public function user()
    {
        return $this->belongsTo('User');
    }



}