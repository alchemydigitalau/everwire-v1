<?php

use \Michelf\Markdown;

class Article extends SuperEloquent 
{
	protected $softDelete = true;
    protected $guarded = array('id');

    protected $before_save = array('process_body');

    /**
	* Relationships
	*/
	public function user()
    {
        return $this->belongsTo('User');
    }

    public function channel()
    {
        return $this->belongsTo('Channel');
    }

    public function place()
    {
        return $this->belongsTo('Place');
    }

    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function flags()
    {
        return $this->hasMany('Flag');
    }

    public function payload()
    {
        return $this->morphTo();
    }
	
    /**
	* Other Methods
	*/
	   public function process_body()
    {
        if( isset($this->attributes['body_md']))
        {
            $this->attributes['body_html'] = Markdown::defaultTransform($this->attributes['body_md']);
        }
    }


    public function getDates()
	{
	    return array('created_at', 'updated_at', 'published_at');
	}

}