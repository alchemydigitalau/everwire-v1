<?php

class Gallery extends Eloquent 
{
	protected $table = 'galleries';
	protected $softDelete = true;
    protected $guarded = array('id');

    /**
	* Relationships
	*/
    public function user()
    {
        return $this->belongsTo('User');
    }

    public function articles()
    {
        return $this->morphMany('Article', 'payload');
    }

    public function images()
    {
        return $this->hasMany('Image');
    }


}