<?php

class Image extends Eloquent 
{
	protected $softDelete = true;
    protected $guarded = array('id');

    /**
	* Relationships
	*/
    public function user()
    {
        return $this->belongsTo('User');
    }

    public function gallery()
    {
        return $this->belongsTo('Gallery');
    }


}