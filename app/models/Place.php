<?php

class Place extends SuperEloquent 
{
    protected $guarded = array('id');
    public $timestamps = false;
    protected $before_save = array('set_name');

    /**
	* Relationships
	*/
    public function articles()
    {
        return $this->hasMany('Article');
    }

    public function users()
    {
        return $this->belongsToMany('User');
    }

    /**
	* Methods
	*/

	public function set_name()
	{
		if( ! empty($this->attributes['full_name']))
        {	
          	$name = explode(',', $this->attributes['full_name']);

          	$this->attributes['name'] = trim(array_shift($name));

        }
	}
}