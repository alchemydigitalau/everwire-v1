<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends SuperEloquent implements UserInterface, RemindableInterface {

	protected $softDelete = true;
	protected $guarded = array('id');

	protected $before_save = array('separate_name', 'hash_password');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	/**
	* Relationships
	*/
	public function profiles()
    {
        return $this->hasMany('Profile');
    }

	public function channels()
    {
        return $this->hasMany('Channel');
    }

    public function articles()
    {
        return $this->hasMany('Article');
    }

    public function comments()
    {
        return $this->hasMany('Comment');
    }

    public function flags()
    {
        return $this->hasMany('Flag');
    }

    public function adverts()
    {
        return $this->hasMany('Advert');
    }

    public function galleries()
    {
        return $this->hasMany('Gallery');
    }

    public function images()
    {
        return $this->hasMany('Image');
    }

    public function videos()
    {
        return $this->hasMany('Video');
    }

    public function places()
    {
        return $this->belongsToMany('Place');
    }


	/**
	* Other Methods
	*/

	public function hash_password()
	{
		if(isset($this->attributes['password']))
		{
			if($this->attributes['password'] != $this->getOriginal('password'))
			{
				$this->attributes['password'] = Hash::make($this->attributes['password']);
			}
		}
	}

	public function separate_name()
	{
		if( ! empty($this->attributes['name']))
        {	
          	$name = explode(' ', $this->attributes['name']);

          	$this->attributes['first_name'] = array_shift($name);
			$this->attributes['last_name'] = implode(' ', $name);

			unset($this->attributes['name']);
        }
	}

}