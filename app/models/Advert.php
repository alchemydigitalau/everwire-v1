<?php

class Advert extends Eloquent 
{
	protected $softDelete = true;
    protected $guarded = array('id');

    /**
	* Relationships
	*/
    public function user()
    {
        return $this->belongsTo('User');
    }


}