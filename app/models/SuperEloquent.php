<?php

class SuperEloquent extends Eloquent 
{
	public function __construct($attributes = array())
	{
	  parent::__construct($attributes);
	}

	public function save(array $options = array())
	{
		if( ! empty($this->before_save))
		{
			foreach($this->before_save as $func)
			{
				if(method_exists($this, $func))
				{
					$this->$func();
				}
			}
		}

		return parent::save($options);
	}
}