<?php

class Channel extends Eloquent 
{
	protected $softDelete = true;
    protected $guarded = array('id');

    /**
	* Relationships
	*/
	public function user()
    {
        return $this->belongsTo('User');
    }

    public function articles()
    {
        return $this->hasMany('Article');
    }
	


}