@extends('layouts/default')

@section('head')
	@parent
	<link href="//vjs.zencdn.net/4.2/video-js.css" rel="stylesheet">
	<script src="//vjs.zencdn.net/4.2/video.js"></script>
@stop

@section('foundation')
	@parent
	<script type="text/javascript" src="{{ asset('js/foundation/foundation.reveal.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/foundation/foundation.interchange.js') }}"></script>

	<script type="text/javascript">
  // Once the video is ready
  
  _V_("example_video_1").ready(function(){

    var aspectRatio = 9/16; // Make up an aspect ratio

    function resizeVideoJS(){
      console.log('foo');
      // Get the parent element's actual width
      var width = $('#example_video_1').parent().width();
      // Set width to fill parent element, Set height
      $('#example_video_1').width(width).height( width * aspectRatio );
    }

    resizeVideoJS(); // Initialize the function
    window.onresize = resizeVideoJS; // Call the function on resize
  });
</script>
@stop

@section('content')
<div class="row">


<div class="column">
<h1>{{ $article->title }}</h1>

<h4 class="subheader">{{ $article->subtitle }}</h4>

<p>
	@if( ! empty($article->user->avatar))
		<img src="{{ $article->user->avatar }}">
		@else
		<img src="http://placehold.it/30x30/A92B48/fff" />
		@endif
	{{ $article->user->first_name }} {{ $article->user->last_name }} in <a href="{{ action('ChannelController@show', $article->channel->id) }}">{{ $article->channel->name }}</a>	
</p>

<hr />




<div class="row">
	<div class="column large-10 medium-9 large-push-2 medium-push-3">

			@if( ! empty($article->payload->url_mp4_high))
			<video id="example_video_1" class="video-js vjs-default-skin"
			  controls preload="auto" 
			  poster="{{ $article->payload->url_preview_thumb }}"
			  data-setup='{"example_option":true}'>
			 <source src="{{ $article->payload->url_mp4_high }}" type='video/mp4' />
			 <source src="{{ $article->payload->url_web }}" type='video/webm' />
			 <source src="{{ $article->payload->url_ogg }}" type='video/ogg' />
			</video>
			@else
			<div class="panel callout">
				Video Encoding...
			</div>
			@endif

		{{ $article->body_html }}
	</div>

	<div class="column large-2 medium-3 large-pull-10 medium-pull-9 ">
		<img src="http://placehold.it/220x220/43ac6a/fff" data-interchange="[http://placehold.it/640x200/43ac6a/fff, (small)], [http://placehold.it/220x220/43ac6a/fff, (medium)], [http://placehold.it/140x140/43ac6a/fff, (large)]" />

		<h5>Advert</h5><p>Complete with descriptive text and a <a href="#">link</a></p>
	</div>
</div>



@include('partials/article_common_block')
@stop