<div class="row" id="comment{{ $comment->id }}">
	<div class="column large-1 medium-1"> 
		@if( ! empty($comment->user->avatar))
		<img src="{{ $comment->user->avatar }}" class="avatar">
		@else
		<img src="http://placehold.it/50x50/A92B48/fff" class="avatar" />
		@endif
	</div>
	
	<blockquote class="column large-11 medium-11">
	{{ $comment->body }}
	<cite>{{ $comment->user->first_name }} {{ $comment->user->last_name }}</cite>
	</blockquote>
</div>