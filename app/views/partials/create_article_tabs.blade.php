@section('create_article_tabs')
<dl class="tabs">
    <dd{{ (Route::currentRouteName() == 'create_article') ? ' class="active"' : '' }}><a href="{{ action('ArticleController@create') }}">Text Article</a></dd>
    <dd{{ (Route::currentRouteName() == 'create_slideshow') ? ' class="active"' : '' }}><a href="{{ action('GalleryController@create') }}">Image Slideshow</a></dd>
    <dd{{ (Route::currentRouteName() == 'create_video') ? ' class="active"' : '' }}><a href="{{ action('VideoController@create') }}">Video</a></dd>
    <dd><a href="#">Event</a></dd>
</dl>

<hr />
@show