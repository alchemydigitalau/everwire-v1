@section('article_common_block')
<div class="row">

	<div class="column large-6 medium-6">
		<hr />
		<h6>Written by</h6>

		@if( ! empty($article->user->avatar))
		<img src="{{ $article->user->avatar }}">
		@else
		<img src="http://placehold.it/50x50/A92B48/fff" />
		@endif

		<h5>{{ $article->user->first_name }} {{ $article->user->last_name }}</h5>
		<time >{{ $article->published_at->format('l jS F Y h:i A') }}</time>
	</div>

	<div class="column large-6 medium-6">
		<hr />
		<h6>Published in</h6>

		<h5><a href="{{ action('ChannelController@show', $article->channel->id) }}">{{ $article->channel->name }}</a></h5>

		<p>Channel description goes here...</p>

		<button class="alert" data-reveal-id="flagModal">Flag this article</button>
	</div>

</div>

<hr />

<h2>Comments</h2>

<div class="row">
	<div class="column column large-10 medium-9">
		<div id="comments">
			@foreach($article->comments as $comment)
			@include('partials/comment', $comment)
			@endforeach
		</div>

		@if (Auth::check())
		{{ 
			Form::open(array(
						'action'	=> array('ArticleController@handleComment', $article->id),
						'onSubmit'	=> 'submitComment(this); return false;',
						'data-abide'
						)) 
		}}
			{{ Form::hidden('user_id', Auth::user()->id) }}

			<label>Add Your Comment:</label>
			<textarea name="body" required></textarea>
			<small class="error">A comment is required</small>
			<button type="submit">Submit</button>
		</form>

		<script type="text/javascript">
			<!--
			function submitComment(form)
			{
				$('button', form).attr('disabled', true);

				$.post('{{ action('ArticleController@handleComment', $article->id) }}',
			            $(form).serialize(), 
			            function(data) {
			            	console.log(data);

			            	

			            	$('#comments').append(data);

			            	$('textarea', form).val('');
			            	$('button', form).removeAttr('disabled');

			            });
			}
			-->
		</script>
		@else
		<div class="panel">
		<a href="{{ action('AuthController@login') }}" class="button">Log In to leave a comment</a>

		</div>

		@endif



	</div>

	<div class="column large-2 medium-3">
		<img src="http://placehold.it/220x440/43ac6a/fff" data-interchange="[http://placehold.it/640x200/43ac6a/fff, (small)], [http://placehold.it/220x440/43ac6a/fff, (medium)], [http://placehold.it/140x280/43ac6a/fff, (large)]" />

		<h5>Another Advert</h5><p>Complete with descriptive text and a <a href="#">link</a></p>
	</div>
</div>


</div>


<div id="flagModal" class="reveal-modal" data-reveal>
	<h2>Flag this Article</h2>
	
	@if(Auth::check())
	{{
		Form::open(array(
					'action' => 'FlagController@handleCreate',
					'data-abide'
					))
	}}

	{{ Form::hidden('article_id', $article->id) }}
	{{ Form::hidden('user_id', Auth::user()->id) }}

	<label>{{ Form::radio('inappropriate', 1, TRUE) }} Article is inappropriate</label>
	<label>{{ Form::radio('inappropriate', 0) }} Article is inaccurate</label>

	<label>Details</label>
	<textarea name="reason" required></textarea>
	<small class="error">Please give more details on why you are flagging this article.</small>

	<button type="submit">Submit</button>

	</form>
	@else
	<a href="{{ action('AuthController@login') }}" class="button">Log In to Flag this Article</a>
	@endif


	<a class="close-reveal-modal">&#215;</a>
</div>

</div>

@show