@section('footer')

<footer class="row text-center">
	<hr />
	&copy; Everwire Ltd. {{ date('Y') }}
	&nbsp;&nbsp;<a href="{{ action('PageController@about') }}">About everwire</a>
	&nbsp;&nbsp;&nbsp;<a href="{{ action('PageController@terms') }}">Terms and Conditions</a>
</footer>
@show