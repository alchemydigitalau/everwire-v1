@section('header')
<div class="contain-to-grid">
	<nav class="top-bar" data-topbar data-options="is_hover: false">
		<ul class="title-area">
			<li class="name">
				<h1><a href="{{ action('HomeController@index') }}">everwire</a></h1>
				
			</li>
			<li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
		</ul>

		<section class="top-bar-section">
			
			<ul class="right">
				@if (Auth::check())
				<li class="active"><a href="{{ action('ArticleController@create') }}">New Article</a></li>
				<li class="has-dropdown">
					<a href="#">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</a>
					<ul class="dropdown">
						<li><a href="{{ action('ChannelController@yourChannels') }}">Your Channels</a></li>
						<li><a href="{{ action('ArticleController@yourArticles') }}">Your Articles</a></li>
						<li><a href="{{ action('AccountController@profile') }}">Settings</a></li>
						<li><a href="{{ action('AuthController@logout') }}">Logout</a></li>
					</ul>
				</li>

					@if(Auth::user()->admin)
					<li class="has-dropdown">
						<a href="#">Admin</a>
						<ul class="dropdown">
							<li><a href="{{ action('FlagController@index') }}">Flagged Articles</a></li>
						</ul>
					</li>

					@endif
				@else
				<li><a href="{{ action('AuthController@login') }}">Login</a></li>
				<li class="divider"></li>
				<li><a href="{{ action('AuthController@signup') }}">Sign Up</a></li>
				@endif
			</ul>

			<!-- Left Nav Section -->
			<ul class="left">
				<li><a href="{{ action('ChannelController@index') }}">Channels</a></li>
			</ul>
		</section>
	</nav>
</div>

@show