@extends('layouts/default')

@section('body')
<body class="auth">
@stop

@section('content')
<div class="row">

<div class="large-6 medium-6 column large-centered medium-centered panel overlay push-top">

<h2>Join everwire today</h2>

<h4 class="subheader">It's completely FREE!</h4>

<hr />

<a href="{{ action('AuthController@facebook') }}" class="button facebook expand">Sign Up with Facebook</a>

{{ Form::open(array(
					'action'	=> 'AuthController@handleSignup'					
					)
					) }}



     @include('partials/form_errors')

	<label>Your Name</label>
    {{ Form::text('name', Input::get('name')) }}

	<label>Email Address</label>
    {{ Form::text('email', Input::get('email')) }}
   
    <label>Password</label>
    {{ Form::password('password') }}

    <label>{{ Form::checkbox('accept_terms', 1) }} I agree to the Everwire Terms &amp; Conditions</label>

    <button type="submit">Sign Up</button>

</form>

</div>

</div>
@stop

@section('foot')
@stop