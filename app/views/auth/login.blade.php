@extends('layouts/default')

@section('body')
<body class="auth">
@stop

@section('content')
<div class="row">
<div class="large-6 medium-6 column large-centered medium-centered panel overlay push-top">

<h2>Log In</h2>

<hr />

<a href="{{ action('AuthController@facebook') }}" class="button facebook expand">Log In with Facebook</a>

{{ Form::open(array(
					'action'	=> 'AuthController@handleLogin'					
					)
					) }}




	 @include('partials/form_errors')

	<label>Email Address</label>
    {{ Form::text('email', Input::get('email')) }}
   
    <label>Password</label>
    {{ Form::password('password') }}

    <button type="submit">Log In</button>

</form>

</div>

</div>
@stop

@section('foot')
@stop