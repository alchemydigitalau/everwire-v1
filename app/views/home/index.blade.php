@extends('layouts/default')

@section('head')
	@parent


	<script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDI2I5xo_amHtzEVkYlp9ZZVSpGDu3FvbQ&sensor=true&libraries=places">
    </script>
    <script type="text/javascript">
    	$(function() {
    		initialize();

    		

		});

        function initialize() {

        	/*geocoder = new google.maps.Geocoder();

        	geocoder.geocode( { 'address': 'Griffin, QLD, Australia'}, function(results, status) {
		      if (status == google.maps.GeocoderStatus.OK) {
		        alert(results[0].geometry.location);
		        
		      } else {
		        alert("Geocode was not successful for the following reason: " + status);
		      }
		    });*/

			var input = document.getElementById('pac-input');

			var autocomplete = new google.maps.places.Autocomplete(input);
			autocomplete.setTypes(['geocode']);

			var ldn = new google.maps.LatLng(51.50960920000001, -0.07610439999996288);

			var griffin = new google.maps.LatLng(-27.2636253, 153.03031839999994);
			var initialLocation;
			var browserSupportFlag =  new Boolean();

			var styles = [
			{
			  stylers: [
			    { hue: "#00ffe6" },
			    { saturation: -20 }
			  ]
			},{
			  featureType: "road",
			  elementType: "geometry",
			  stylers: [
			    { lightness: 100 },
			    { visibility: "simplified" }
			  ]
			}
			];

			var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});

			var mapOptions = {
				zoom: 14,
				zoomControl: false,
				streetViewControl: false,
				panControl: false,
				mapTypeControl: false,
				mapTypeControlOptions: {
				  mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
				}
			};

			var map = new google.maps.Map(document.getElementById('gmap'), mapOptions);

			map.mapTypes.set('map_style', styledMap);
			map.setMapTypeId('map_style');

			if(navigator.geolocation) {
				browserSupportFlag = true;
				navigator.geolocation.getCurrentPosition(function(position) {
				//initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
				map.setCenter(new google.maps.LatLng(position.coords.latitude,position.coords.longitude));
			}, function() {
				handleNoGeolocation(browserSupportFlag);
				});
			}
			 // Browser doesn't support Geolocation
			else {
				browserSupportFlag = false;
				handleNoGeolocation(browserSupportFlag);
			}

			function handleNoGeolocation(errorFlag) {
			    if (errorFlag == true) {
			      initialLocation = ldn;
			    } else {
			      initialLocation = ldn;
			    }
			    map.setCenter(ldn);
			  }
        }
    </script>
@stop

@section('content')
<div class="masthead">
	<div id="gmap"></div>
	<div class="row">
		<div class="push-top medium-6 medium-centered columns panel overlay">
			<h2>Search locally</h2>
			<form>

			<input type="text" name="p" id="pac-input" placeholder="Where are you?" />

			<button class="expand">Go!</button>
			</form>
		</div>
	</div>

</div>


<div class="row">



<div class="column large-2 medium-3 large-push-10 medium-push-9 advert">
	<img src="http://placehold.it/220x440/43ac6a/fff" data-interchange="[http://placehold.it/640x200/43ac6a/fff, (small)], [http://placehold.it/220x440/43ac6a/fff, (medium)], [http://placehold.it/140x280/43ac6a/fff, (large)]" />

	<h5>Advert</h5><p>Complete with descriptive text and a <a href="#">link</a></p>
</div>


<div class="column large-10 medium-9 large-pull-2 medium-pull-3 ">
<h1>Latest Articles</h1>

@foreach($articles as $article)
<div class="row">
	<div class="column small-10">
		<h3><a href="{{ action('ArticleController@show', $article->id) }}">{{ $article->title }}</a></h3>
		<h6 class="subheader">{{ $article->subtitle }}</h6>

		{{ $article->user->first_name }} {{ $article->user->last_name }} in <a href="{{ action('ChannelController@show', $article->channel->id) }}">{{ $article->channel->name }}</a> | {{ $article->place->name }} | <time>{{ $article->published_at->diffForHumans() }}</time>

	</div>

	<div class="column small-2 text-center">
		@if( ! empty($article->user->avatar))
		<img src="{{ $article->user->avatar }}" class="avatar">
		@else
		<img src="http://placehold.it/50x50/A92B48/fff" class="avatar" />
		@endif

		
	</div>

</div>

<hr />
@endforeach

@if( ! empty($articles))
{{ $articles->links() }}
@endif
</div>

</div>
@stop