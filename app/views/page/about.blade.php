@extends('layouts/default')

@section('content')
<div class="row">

<div class="column">
	<h1>About everwire</h1>

	<h4 class="subheader">Here we'll add some information about what everwire actually is...</h4>

	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id vehicula leo. Nulla facilisi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis tincidunt elit at dui ultricies hendrerit. Mauris pellentesque arcu quis sem tristique, sit amet aliquet odio lobortis. Sed metus erat, imperdiet viverra varius in, porta congue libero. Suspendisse est metus, malesuada vitae elit in, posuere tristique felis. Nunc lacus ipsum, hendrerit id eros ac, cursus auctor nibh. Sed vestibulum urna pretium tempus vestibulum. Curabitur non interdum nisl. Pellentesque faucibus est eget ornare adipiscing. Vestibulum et lacus vitae dui dignissim tempor id sed lacus. Suspendisse eleifend pellentesque turpis, sed rutrum tortor ullamcorper in.</p>

	<p>Curabitur pulvinar, mi id tristique ultricies, velit velit sollicitudin justo, eu euismod diam mi ac tortor. Nullam elementum porta nunc euismod vehicula. Nam vel felis elit. Phasellus in auctor libero. Cras volutpat lacinia magna, sit amet volutpat turpis tempor nec. Aenean fringilla erat ac ligula luctus, eget feugiat metus rhoncus. Sed diam tellus, auctor euismod libero nec, aliquam laoreet odio. Pellentesque diam erat, faucibus vitae arcu in, gravida commodo tortor.</p>

	<p>Proin facilisis luctus hendrerit. Etiam at vestibulum magna. Integer eget lacus eget turpis pharetra ornare non dapibus eros. Nunc est tellus, dapibus sed enim eu, facilisis commodo eros. Fusce viverra enim sit amet blandit tempus. Suspendisse congue nec velit sit amet egestas. Proin nec libero ac nunc euismod euismod a eu nisi. Proin a semper metus. Maecenas a interdum augue.</p>

	<p>Donec et blandit metus. Sed rutrum at elit ac viverra. Proin quis tempor dui, vel commodo nunc. Aliquam et urna justo. Sed mollis ligula sem, quis congue sapien eleifend eget. Nam tempor quam nec urna malesuada commodo. Aliquam aliquam tempor purus eget auctor. Aliquam erat volutpat. Nunc mi augue, malesuada volutpat magna at, eleifend pharetra neque. Duis convallis eros nibh, vitae aliquet eros viverra et. Etiam egestas velit ut diam mollis, non euismod magna fringilla. Etiam ut lorem sed velit vehicula placerat sit amet in mi.</p>

	<p>Nulla consectetur feugiat quam. Fusce at lorem ut leo interdum facilisis. Vestibulum eget erat interdum, tempor sapien sit amet, semper libero. Duis accumsan vel mauris ut placerat. Donec id placerat elit. Nulla facilisi. Suspendisse vel lorem ac nisl laoreet porta. Mauris vulputate sapien tellus, non tincidunt tellus bibendum non. Nam tempus fermentum justo, eget cursus turpis cursus eget. Cras euismod vestibulum mi, non pretium enim pellentesque sit amet. Pellentesque ut elit libero. Nunc nec interdum dui.</p>


</div>

</div>
@stop