@extends('layouts/default')

@section('content')
<div class="row">

<div class="column">
<h1>Your Channels</h1>

<a href="{{ action('ChannelController@create') }}" class="button">New Channel</a>

<table class="column">
	<thead>
		<tr>
			<td>Name</td>
			<td>Articles</td>
			<td></td>
		</tr>
	</thead>

	<tbody>
		@foreach($channels as $channel)
		<tr>
			<td><a href="{{ action('ChannelController@show', $channel->id) }}">{{ $channel->name }}</a></td>
			<td>{{ $channel->articles()->wherePublished(true)->count() }}</td>
			<td><a href="{{ action('ChannelController@edit', $channel->id) }}">Edit</a></td>
		</tr>
		@endforeach
	</tbody>
</table>

</div>

</div>
@stop