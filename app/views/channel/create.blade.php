@extends('layouts/default')

@section('content')
<div class="row">

<div class="column">
<h1>Create a New Channel</h1>
{{
Form::open(array(
				'action' => 'ChannelController@handleCreate'
				))
}}
    @include('partials/form_errors')
    
	<label>Name</label>
    {{ Form::text('name', Input::get('name')) }}

    <button type="submit">Create Channel</button>
</form>

</div>

</div>
@stop