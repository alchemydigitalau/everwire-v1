@extends('layouts/default')

@section('content')
<div class="row">

<div class="column">
<h1>Edit Channel</h1>
{{
Form::model(
            $channel,
            array(
				'action' => array('ChannelController@handleEdit', $channel->id)
				))
}}
    @include('partials/form_errors')
    
	<label>Name</label>
    {{ Form::text('name', Input::get('name')) }}

    <button type="submit">Save Changes</button>
</form>

</div>

</div>
@stop