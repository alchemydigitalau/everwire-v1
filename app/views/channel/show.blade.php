@extends('layouts/default')

@section('content')
<div class="row">

<div class="column">
<h1>{{ $channel->name }}</h1>

<h2>Latest Articles</h2>
</div>

<div class="column large-2 medium-3 large-push-10 medium-push-9 ">
	<img src="http://placehold.it/220x440/43ac6a/fff" data-interchange="[http://placehold.it/640x200/43ac6a/fff, (small)], [http://placehold.it/220x440/43ac6a/fff, (medium)], [http://placehold.it/140x280/43ac6a/fff, (large)]" />

	<h5>Advert</h5><p>Complete with descriptive text and a <a href="#">link</a></p>
</div>


<div class="column large-10 medium-9 large-pull-2 medium-pull-3 ">

@foreach($articles as $article)
<div class="row">
	<div class="column small-10">
		<h3><a href="{{ action('ArticleController@show', $article->id) }}">{{ $article->title }}</a></h3>
		<h6 class="subheader">{{ $article->subtitle }}</h6>

		{{ $article->user->first_name }} {{ $article->user->last_name }} | <time>{{ $article->published_at->diffForHumans() }}</time>

	</div>

	<div class="column small-2 text-center">
		@if( ! empty($article->user->avatar))
		<img src="{{ $article->user->avatar }}" class="avatar">
		@else
		<img src="http://placehold.it/50x50/A92B48/fff" class="avatar" />
		@endif

		
	</div>

</div>
<hr />
@endforeach

</div>

</div>
@stop