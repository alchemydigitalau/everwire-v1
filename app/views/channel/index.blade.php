@extends('layouts/default')

@section('content')
<div class="row">

<div class="column">
<h1>All Channels</h1>
</div>


@foreach($channels as $channel)
<div class="column large-3 medium-6">
	<div class="panel">
		<h3><a href="{{ action('ChannelController@show', $channel->id) }}">{{ $channel->name }}</a></h3>
		<p>
			{{ $channel->articles()->wherePublished(true)->count() }} articles
		</p>
	</div>
</div>
@endforeach

</div>
@stop