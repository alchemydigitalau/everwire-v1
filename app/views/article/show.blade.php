@extends('layouts/default')



@section('foundation')
	@parent
	<script type="text/javascript" src="{{ asset('js/foundation/foundation.reveal.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/foundation/foundation.orbit.js') }}"></script>
@stop

@section('content')
<div class="row">


	<div class="column">
	<h1>{{ $article->title }}</h1>

	<h4 class="subheader">{{ $article->subtitle }}</h4>

	<p>
		@if( ! empty($article->user->avatar))
			<img src="{{ $article->user->avatar }}" class="avatar">
			@else
			<img src="http://placehold.it/30x30/A92B48/fff" class="avatar" />
			@endif
		{{ $article->user->first_name }} {{ $article->user->last_name }} in <a href="{{ action('ChannelController@show', $article->channel->id) }}">{{ $article->channel->name }}</a>	
	</p>

	<hr />

	<div class="row">
		<div class="column large-10 medium-9 large-push-2 medium-push-3">
			{{ $article->body_html }}
		</div>

		<div class="column large-2 medium-3 large-pull-10 medium-pull-9 ">
			<img src="http://placehold.it/220x220/43ac6a/fff" data-interchange="[http://placehold.it/640x200/43ac6a/fff, (small)], [http://placehold.it/220x220/43ac6a/fff, (medium)], [http://placehold.it/140x140/43ac6a/fff, (large)]" />

			<h5>Advert</h5><p>Complete with descriptive text and a <a href="#">link</a></p>
		</div>
	</div>



	@include('partials/article_common_block')
@stop