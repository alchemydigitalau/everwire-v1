@extends('layouts/default')

@section('content')
<div class="row">

	<div class="column">
	<h1>Your Articles</h1>


	<a href="{{ action('ArticleController@create') }}" class="button">New Article</a>

	<table class="column">
	<thead>
	<tr>
		<td>Title</td>
		<td>Channel</td>
		<td>Published</td>
		<td>Comments</td>
		<td></td>
	</tr>
	</thead>

	<tbody>
		@foreach($articles as $article)
		<tr>
		<td><a href="{{ action('ArticleController@show', $article->id) }}">{{ $article->title }}</a></td>
		<td><a href="{{ action('ChannelController@show', $article->channel->id) }}">{{ $article->channel->name }}</a></td>
		<td>@if ( ! empty($article->published_at)) {{ $article->published_at->format('l jS F Y h:i A') }} @endif</td>
		<td>{{ $article->comments()->count() }}</td>
		<td><a href="{{ action('ArticleController@edit', $article->id) }}">Edit</a></td>
		</tr>
	@endforeach

	</tbody>

	</table>

	</div>

</div>
@stop