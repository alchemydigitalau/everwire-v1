@extends('layouts/default')

@section('head')
    @parent


    <script type="text/javascript"
      src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDI2I5xo_amHtzEVkYlp9ZZVSpGDu3FvbQ&sensor=true&libraries=places">
    </script>

    <script type="text/javascript">
        $(function() {
            initialize();
        });

        function initialize() {
            var input = document.getElementById('pac-input');

            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.setTypes(['geocode']);

        }

    </script>
@stop

@section('content')
<div class="row">

    <div class="column">
    <h1>Create a New Article</h1>

    @include('partials/create_article_tabs')

    {{
    Form::open(array(
                    'action' => 'ArticleController@handleCreate'
                    ))
    }}
        @include('partials/form_errors')

        <label>Title</label>
        {{ Form::text('title', Input::get('title')) }}

        <label>Subtitle</label>
        {{ Form::text('subtitle', Input::get('subtitle')) }}

        <label>Channel</label>
        {{ Form::select('channel_id', $channels) }}

        <label>Content</label>
        {{ Form::textarea('body_md', Input::get('body_md'), array('style'=> 'height: 300px;')) }}

        <label>Location</label>
        @foreach(Auth::user()->places as $place)
        <label>{{ Form::radio('place_id', $place->id) }} {{ $place->full_name }}</label>
        @endforeach

        <label>Or a New Location</label>
        {{ Form::text('place', null, array('id' => 'pac-input')) }}

        <label for="published"><input id="published" type="checkbox" name="published"> Publish this article</label>

        <button type="submit">Save Article</button>
    </form>

    </div>
</div>
@stop