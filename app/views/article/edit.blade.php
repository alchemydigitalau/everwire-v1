@extends('layouts/default')

@section('content')
<div class="row">

    <div class="column">
    <h1>Edit Article</h1>
    {{
    Form::model(
    			$article,
    			array(
    				'action' => array('ArticleController@handleEdit', $article->id)
    				))
    }}

    	@include('partials/form_errors')

    	<label>Title</label>
        {{ Form::text('title', Input::get('title')) }}

        <label>Subtitle</label>
        {{ Form::text('subtitle', Input::get('subtitle')) }}

        <label>Channel</label>
        {{ Form::select('channel_id', $channels, Input::get('channel_id')) }}

        <label>Content</label>
        {{ Form::textarea('body_md', Input::get('body_md'), array('style'=> 'height: 300px;')) }}

        <label>{{ Form::checkbox('published') }} Publish this article</label>

        <button type="submit">Save Changes</button>
    </form>

    </div>

</div>
@stop