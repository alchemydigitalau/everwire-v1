@extends('layouts/default')

@section('content')
<div class="row">

	@include('partials/account_sidebar')

	<div class="column large-10 medium-9">
	<h1>Change Password</h1>
	{{
	Form::open(
	            array(
					'action' => 'AccountController@handlePassword'
					))
	}}
	    @include('partials/form_errors')

		<label>New Password</label>
	    {{ Form::password('password') }}

	    <label>Confirm New Password</label>
	    {{ Form::password('password_confirmation') }}

	    <button type="submit">Save Changes</button>
	</form>

	</div>

</div>
@stop