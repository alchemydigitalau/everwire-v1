@extends('layouts/default')

@section('content')
<div class="row">

    @include('partials/account_sidebar')

    <div class="column large-10 medium-9">
    <h1>Edit Profile</h1>
    {{
    Form::model(
                Auth::user(),
                array(
    				'action' => 'AccountController@handleProfile'
    				))
    }}
        @include('partials/form_errors')

    	<label>First Name</label>
        {{ Form::text('first_name', Input::get('first_name')) }}

        <label>Last Name</label>
        {{ Form::text('last_name', Input::get('last_name')) }}

         <label>Email</label>
        {{ Form::text('email', Input::get('email')) }}

        <button type="submit">Save Changes</button>
    </form>

    </div>
</div>
@stop