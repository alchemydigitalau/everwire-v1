<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >

<head>
	@section('head')
	<meta charset="utf-8">
	<!-- Always force latest IE rendering engine & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title>everwire</title>
	
	<!-- Mobile Viewport Fix -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

	<script type="text/javascript" src="//use.typekit.net/xvl0aba.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	
	<link href="{{ asset('css/normalize.css') }}" type="text/css" rel="stylesheet" />
	<link href="{{ asset('css/foundation.min.css') }}" type="text/css" rel="stylesheet" />

	<script type="text/javascript" src="{{ asset('js/vendor/custom.modernizr.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/vendor/jquery.js') }}"></script>

	@show
</head>

@section('body')
<body>
@show

	@include("partials/header")

	@yield("content")

	@section('foot')
	@include("partials/footer")
	@show


	@section('foundation')
	<script type="text/javascript" src="{{ asset('js/foundation/foundation.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/foundation/foundation.topbar.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/foundation/foundation.interchange.js') }}"></script>
	@show

	<script type="text/javascript">
	<!--
		$(document).foundation();
	-->
	</script>
</body>
</html>