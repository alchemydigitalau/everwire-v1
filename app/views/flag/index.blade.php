@extends('layouts/default')

@section('content')
<div class="row">

<div class="column">
    <h1>Flagged Articles</h1>

    <table class="column">
    <tbody>
    @foreach($flags as $flag)
    <tr>
    <td>{{ $flag->article->title }}</td>

    </tr>

    @endforeach
    </tbody>

    </table>

</div>

</div>
@stop